SDL2CFFI - A Python interface to the SDL2 library using CFFI.

Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
All rights reserved.

Licensed under Apache License, Version 2.0.
See LICENSE.txt for licensing details.
