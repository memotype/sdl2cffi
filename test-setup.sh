#!/bin/bash -i

venv="venv-$RANDOM"
mkvirtualenv "$venv"
python setup.py install
if python -c "import sdl2cffi"; then
    echo
    echo "+++ Import succeeded."
    echo
else
    echo
    echo "!!! Import failed!"
    echo
fi
deactivate
rmvirtualenv "$venv"
