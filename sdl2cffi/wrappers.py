# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


from collections import Sequence

from cffiwrap import CObject, cmethod, cproperty, cstaticmethod

from sdl2cffi.api import *
from sdl2cffi.util import *


__all__ = [
    'SDLError',
    'Window',
    #'Surface',
    'Texture',
    'Renderer',
    'DrawColor',
]


class SDLError(Exception):
    def __init__(self, msg):
        msg += ' Original SDL error: ' + SDL_GetError()
        super(SDLError, self).__init__(msg)


class SDLObject(CObject):
    def _checkerr(self, cfunc, args, retval):
        if isinstance(retval, Sequence):
            ret = retval[0]
            # TODO Don't think any SDL functions return a negative on success.
            if ret < 0:
                raise SDLError("Got negative return value")
            elif ret == sdl2_ffi.NULL:
                raise SDLError("Got NULL return value")
            return retval[1:]
        else:
            return retval


class DrawColor(object):
    def __init__(self, sdl_obj, color):
        # TODO: doc: sdl_obj can be renderer or texture, or anything with a
        # settable/gettable "color" property.
        self.sdl_obj = sdl_obj
        # TODO: doc: color can be 3 or 4 long, Renderer.color prop handles both
        self.color = color
        self._old_color = sdl_obj.color

    def __enter__(self):
        self._old_color = self.sdl_obj.color
        self.sdl_obj.color = self.color

    def __exit__(self, exctype, value, traceback):
        self.sdl_obj.color = self._old_color


class Window(SDLObject):
    brightness = cproperty(SDL_GetWindowBrightness)
    displayindex = cproperty(SDL_GetWindowDisplayIndex)
    flags = cproperty(SDL_GetWindowFlags)
    renderer = cproperty(SDL_GetRenderer)

    _cnew = cstaticmethod(SDL_CreateWindow)
    _cdel = cmethod(SDL_DestroyWindow)
    get_display_mode = cmethod(SDL_GetWindowDisplayMode, outargs=[1])
    _get_gamma_ramp = cmethod(SDL_GetWindowGammaRamp, arrays=[1,2,3])

    #_props = {
    #    'brightness': SDL_GetWindowBrightness,
    #    'displayindex': SDL_GetWindowDisplayIndex,
    #    'flags': SDL_GetWindowFlags,
    #    'renderer': SDL_GetRenderer,
    #}
    #_meths = {
    #    '_cnew': SDL_CreateWindow,
    #    '_cdel': SDL_DestroyWindow,
    #    'get_display_mode': (SDL_GetWindowDisplayMode, [1]),
    #    '_get_gamma_ramp': (SDL_GetWindowGammaRamp,
    #                       {'arrays': [1, 2, 3]}),
    #}

    def get_gammaramp(self):
        # This fills in 3 arrays, each 256 elements long. (cffiwrap allocates
        # the arrays.)
        return self._get_gammaramp(256, 256, 256)


class Renderer(SDLObject):

    _cnew = cstaticmethod(SDL_CreateRenderer)
    _cdel = cmethod(SDL_DestroyRenderer)
    get_info = cmethod(SDL_GetRendererInfo, [1])
    get_blend_mode = cmethod(SDL_GetRenderDrawBlendMode, [1])
    get_color = cmethod(SDL_GetRenderDrawColor, [1,2,3,4])
    set_color = cmethod(SDL_SetRenderDrawColor)
    present = cmethod(SDL_RenderPresent)
    clear = cmethod(SDL_RenderClear)
    render = cmethod(SDL_RenderCopy)

    #_meths = {
    #    '_cnew': SDL_CreateRenderer,
    #    '_cdel': SDL_DestroyRenderer,
    #    'get_info': (SDL_GetRendererInfo, [1]),
    #    'get_blend_mode': (SDL_GetRenderDrawBlendMode, [1]),
    #    'get_color': (SDL_GetRenderDrawColor, [1,2,3,4]),
    #    'set_color': SDL_SetRenderDrawColor,
    #    'present': SDL_RenderPresent,
    #    'clear': SDL_RenderClear,
    #    'render': SDL_RenderCopy,
    #}

    @property
    def color(self):
        # TODO: _checkerr is broken, need to strip the retval myself for now.
        return self.get_color()[1:4]

    @color.setter
    def color(self, color):
        self.set_color(*color2RGBA(color, alpha=self.alpha))

    @property
    def alpha(self):
        return self.get_color()[4]

    @alpha.setter
    def set_alpha(self, alpha):
        self.set_color(self.color + (alpha,))

    #def create_texture(self, tformat, access, w, h):
    #    return Texture(self, tformat, access, w, h)



class Texture(SDLObject):
    _cnew = cmethod(SDL_CreateTexture)
    _cdel = cmethod(SDL_DestroyTexture)
    from_surface = cstaticmethod(SDL_CreateTextureFromSurface)
    get_alpha = cmethod(SDL_GetTextureAlphaMod, [1])
    set_alpha = cmethod(SDL_SetTextureAlphaMod)
    get_format = cmethod(SDL_QueryTexture, outargs=[1,2,3,4], retargs=[1])
    access = cmethod(SDL_QueryTexture, outargs=[1,2,3,4], retargs=[2])
    get_color = cmethod(SDL_GetTextureColorMod, outargs=[1,2,3])
    set_color = cmethod(SDL_SetTextureColorMod)

    #_meths = {
    #    '_cnew': SDL_CreateTexture,
    #    #'create_texture_from_surface': SDL_CreateTextureFromSurface,
    #    '_cdel': SDL_DestroyTexture,
    #    'get_alpha': (SDL_GetTextureAlphaMod, [1]),
    #    'set_alpha': SDL_SetTextureAlphaMod,
    #    'get_format': (SDL_QueryTexture, {'outargs': [1,2,3,4],
    #                                      'retargs': [1]}),
    #    # TODO: _MetaWrap in cffiwrap calls set_outargs on the function...
    #    #'access': (SDL_QueryTexture, {'outargs': [1,2,3,4], 'retargs': [2]}),
    #    'get_color': (SDL_GetTextureColorMod, {'outargs': [1,2,3],
    #                                           'retargs': [1,2,3]}),
    #    'set_color': SDL_SetTextureColorMod,
    #}
    #_props = {
    #    # TODO: Allow strings as propery values to use things defined under
    #    # _meths above, or support full "outargs" type stuff in _props...
    #}

    def __init__(self, size=None, pformat=SDL_PIXELFORMAT_RGBA8888,
                 access=SDL_TEXTUREACCESS_STREAMING, surface=None,
                 imgfile=None, colorkey=(255,0,255)):

        if size is None and imgfile is None and surface is None:
            raise ValueError('Must give size, surface or imgfile')
            
        self.surface = surface
        self.size = size

        free_surf = False
        if imgfile is not None:
            free_surf = True
            self.surface = IMG_Load(imgfile)
            if self.surface == NULL:
                raise IMGError(IMG_GetError[0])

        if self.surface is not None:
            # TODO _props
            self.w, self.h = self.surface.w, self.surface.h
            if colorkey is not None:
                key = SDL_MapRGB(self.surface.format, *colorkey)
                SDL_SetColorKey(self.surface, 1, key)

    def _init_texture(self, renderer):
        self.renderer = renderer
        if self.surface is not None:
            self.texture = SDL_CreateTextureFromSurface(self.renderer,
                                                        self.surface)
            SDL_FreeSurface(self.surface)
            self.surface = None

        elif self.size is not None:
            self.w, self.h = size
            self.texture = SDL_CreateTexture(renderer, pformat, access,
                                                 self.w, self.h)
        else:
            raise ValueError("Somehow the surface wasn't created or the size "
                             "wasn't set. This shouldn't happen.")
            
        self._cdata = self.texture


    @property
    def color(self):
        return self.get_color()

    @color.setter
    def color(self, color):
        # TODO: in cffiwrap, maybe treat tuples to be expanded, and lists to be
        # converted to arrays?
        color = color2RGB(color)
        self.set_color(*color)

    @property
    def alpha(self):
        return self.get_alpha()

    @alpha.setter
    def set_alpha(self, alpha):
        self.set_alpha(alpha)


