# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


# TODO: A lot of this could probably be generalized and refactored to cffiwrap

import os
from cffi import FFI
import cffiwrap as wrap


__all__ = ['NULL', 'sdl2_ffi', 'sdl2_api']


class SDLError(Exception): pass

# TODO: Hack. More flexibility/customizability needed...
_modpath = os.path.dirname(__file__)
_cffi_code  = file(os.path.join(_modpath, 'sdl2.cffi')).read()
_cffi_code += file(os.path.join(_modpath, 'image', 'img.cffi')).read()
_verify_code = '''
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
'''

_include_dirs = [
]

_library_dirs = [
]

_libraries = [
    'SDL2',
    'SDL2_image',
]

define_macros = [
    # TODO A few CPP macros in the header files. Need some way to switch them
    # on at module load time or something?

    # These are used in the SDL_RWops struct
    #'ANDROID',
    #'__WIN32__',
    #'HAVE_STDIO_H',

    # Used at the top of sdl2.cffi for SDLCALL (but CFFI doesn't like #ifndef).
    #'__WIN32__',
    #'__GNUC__',
]

sdl2_ffi = FFI()
sdl2_ffi.cdef(_cffi_code)
sdl2_api = sdl2_ffi.verify(_verify_code,
                           library_dirs=_library_dirs,
                           include_dirs=_include_dirs,
                           libraries=_libraries)

NULL = sdl2_ffi.NULL

_cobjs = wrap.wrapall(sdl2_ffi, sdl2_api)
globals().update(_cobjs)
__all__ += _cobjs.keys()
