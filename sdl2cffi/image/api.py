# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt


import os
from cffi import FFI
import cffiwrap as wrap

from sdl2cffi import api as sdl2api


__all__ = ['NULL']


class IMGError(Exception): pass

_modpath = os.path.dirname(__file__)
#_cffi_code  = sdl2api._cffi_code
_cffi_code = file(os.path.join(_modpath, 'img.cffi')).read()
_verify_code = '''
#include "SDL.h"
#include "SDL_image.h"
'''

_include_dirs  = sdl2api._include_dirs
_include_dirs += [
    os.path.join(_modpath, '../../upstream-src/SDL2_image')
]

_library_dirs  = sdl2api._library_dirs
_library_dirs += [
]

_libraries  = sdl2api._libraries
_libraries += [
    'SDL2',
    'SDL2_image',
]

img_ffi = FFI()
img_ffi.cdef(_cffi_code)
img_api = img_ffi.verify(_verify_code,
                           library_dirs=_library_dirs,
                           include_dirs=_include_dirs,
                           libraries=_libraries)

NULL = img_ffi.NULL

_cfuncs = wrap.CFunction.fromAPI(img_api, img_ffi)
globals().update(_cfuncs)
__all__ += _cfuncs.keys()
