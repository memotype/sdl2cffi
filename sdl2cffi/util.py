# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


from collections import Sequence, deque

from sdl2cffi.api import *


__all__ = [
    'color2RGB',
    'color2RGBA',
    'Timer',
    'Eventerator',
]


def color2RGB(color):
    if isinstance(color, Sequence):
        return color[:3]
    else:
        return (color.r, color.g, color.b)


def color2RGBA(color, alpha=255):
    ''' Accepts a SDL_Color struct and returns a tuple of the RGBA values. '''
    if isinstance(color, Sequence):
        if len(color) == 3:
            color += (alpha,)
        return color
    else:
        return (color.r, color.g, color.b, color.a)


class Timer(object):

    ''' A simple timer object useful for calculating FPS, etc.

    Attributes:
    * ``start``: The time when Timer was initialized.
    * ``fpshist``: The recorded history of ``fps`` calls up to ``maxhist``.
    * ``last``: The last recorded time (e.g. when ``elapsed`` was last called).

    '''

    def __init__(self, fpshist=10, func=SDL_GetTicks):
        ''' Initialize a new timer object.

        * ``fpshist``: The maximum number of values to save in the history each
          time ``fps`` is called. Default is 10.
        * ``func``: A function that returns a time value in milliseconds. Used
          to calculate elapsed time. Defaults to SDL_GetTicks. (The Timer
          object will be as thread safe as this function is.)

        '''

        self._time_func = func
        self.fpshist = deque(maxlen=fpshist)
        self.start = func()
        self.last = self.start

    def reset(self, keephist=False):
        ''' Reset the Timer.

        Resets the ``start`` and ``last`` times to the current time and clears
        the FPS history unless ``keephist`` is True.

        '''

        self.start = self._time_func()
        self.last = self.start
        if not keephist:
            self.hist.clear()

    def elapsed(self, total=False, peek=False):
        ''' Returns the time since ``elapsed`` or ``fps`` were last called.
        
        The first time this is called it returns the time since Timer was
        initialized.

        * ``total``: If True, returns the total elapsed time since the Timer
          was initialized. Default is to return the time since the last call.
        * ``peek``: If True, returns the elapsed time without saving the time
          to the ``last`` attribute. Default is False.

        '''

        last_time = self.last
        cur_time = self._time_func()
        if not peek:
            self.last = cur_time
        if total:
            return cur_time - self.start
        else:
            return cur_time - last_time

    def fps(self, avg=True, peek=False):
        ''' Returns the "frames per second".

        Can actually be used for "<loops/whatevers> per sec". Basically
        calculates the number of calls to ``fps`` per second.

        * ``avg``: If True (default) calculate the average FPS over the past
          ``maxhist`` (as given to ``__init__``) calls. If False just returns
          the FPS of the current call.
        * ``peek``: Passed to ``elapsed`` and also prevents the current FPS
          from being recorded to the history (``fpshist`` attribute).

        Returns a float.

        '''

        elapsed = self.elapsed(peek=peek)
        secs = (elapsed / 1000.0) + 0.00001  # Add to prevent zero division.
        fps = 1.0 / secs

        if not peek:
            self.fpshist.append(fps)

        if avg:
            return sum(self.fpshist)/len(self.fpshist)
        else:
            return fps


class Eventerator(object):
    ''' An event iterator. Get it? '''
    def __init__(self, bufsize=10):
        ''' Create an Eventerator.

        * ``bufsize``: The size of the buffer to use to pull events from SDL.
          Default is 10 and shouldn't really need to be set unless you have
          specific needs.

        Eventerator objects are reusable, just use it in an iteration context
        and all the events on the SDL event queue will be yielded.

        '''

        self._bufsize = bufsize
        self._buff = SDL_Event.array(bufsize)

    def peek(self):
        ''' Iterates over events without popping them from the event stack. '''
        return self.__iter__(peek=True)

    def __iter__(self, peek=False):
        ''' Iterates over events in the SDL event queue. '''

        SDL_PumpEvents()

        if peek:
            peek = SDL_PEEKEVENT
        else:
            peek = SDL_GETEVENT

        nevents = 1
        while nevents > 0:
            nevents = SDL_PeepEvents(self._buff, self._bufsize, peek,
                                     SDL_FIRSTEVENT, SDL_LASTEVENT)
            for event in self._buff[0:nevents]:
                yield event
